#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void    MainWindow::resizeTableWidget(int row, int col)
{
    this->clearTableWidget();

    this->ui->wordsTable->setRowCount(row);
    this->ui->wordsTable->setColumnCount(col);

    for (int nRow = 0; nRow < this->ui->wordsTable->rowCount(); ++nRow)
    {
        this->ui->wordsTable->setHorizontalHeaderItem(nRow, new QTableWidgetItem(QString::number(nRow)));
        for (int nCol = 0; nCol < this->ui->wordsTable->columnCount(); ++nCol)
        {
            this->ui->wordsTable->setVerticalHeaderItem(nCol, new QTableWidgetItem(QString::number(nCol)));
            QTableWidgetItem    *item = new QTableWidgetItem();
            this->ui->wordsTable->setItem(nRow, nCol, item);
        }
    }
}

void        MainWindow::clearTableWidget()
{
    for (int nRow = 0; nRow < this->ui->wordsTable->rowCount(); ++nRow)
    {
        for (int nCol = 0; nCol < this->ui->wordsTable->columnCount(); ++nCol)
            delete this->ui->wordsTable->item(nRow, nCol);
    }

    this->ui->wordsTable->clear();
    this->ui->wordsTable->setRowCount(0);
    this->ui->wordsTable->setColumnCount(0);
}

void MainWindow::on_horizontalPushButton_clicked()
{
    int                 newRowCount = this->ui->horizontalSpinBox->value();
    int                 rowCount = this->ui->wordsTable->rowCount();

    if (newRowCount > rowCount)
    {
        this->ui->wordsTable->setRowCount(newRowCount);
        while (rowCount != newRowCount)
        {
            this->ui->wordsTable->setHorizontalHeaderItem(rowCount, new QTableWidgetItem(QString::number(rowCount)));
            for (int column = 0; column < this->ui->wordsTable->columnCount(); ++column)
            {
                this->ui->wordsTable->setVerticalHeaderItem(column, new QTableWidgetItem(QString::number(column)));
                QTableWidgetItem    *item = new QTableWidgetItem();
                this->ui->wordsTable->setItem(rowCount, column, item);
            }
            ++rowCount;
        }
    }
    else if (newRowCount < rowCount)
    {
        while (rowCount != newRowCount)
        {
            for (int column = 0; column < this->ui->wordsTable->columnCount(); ++column)
                delete this->ui->wordsTable->item(rowCount - 1, column);
            --rowCount;
        }
        this->ui->wordsTable->setRowCount(newRowCount);
    }
    else
        return ;
}

void MainWindow::on_verticalPushButton_clicked()
{
    int                 newColumnCount = this->ui->verticalSpinBox->value();
    int                 columnCount = this->ui->wordsTable->columnCount();

    if (newColumnCount > columnCount)
    {
        this->ui->wordsTable->setColumnCount(newColumnCount);
        while (columnCount != newColumnCount)
        {
            this->ui->wordsTable->setVerticalHeaderItem(columnCount, new QTableWidgetItem(QString::number(columnCount)));
            for (int row = 0; row < this->ui->wordsTable->rowCount(); ++row)
            {
                QTableWidgetItem    *item = new QTableWidgetItem();
                this->ui->wordsTable->setItem(columnCount, row, item);
            }
            ++columnCount;
        }
    }
    else if (newColumnCount < columnCount)
    {
        while (columnCount != newColumnCount)
        {
            for (int row = 0; row < this->ui->wordsTable->rowCount(); ++row)
                delete this->ui->wordsTable->item(columnCount - 1, row);
            --columnCount;
        }
        this->ui->wordsTable->setColumnCount(newColumnCount);
    }
    else
        return ;
}

void MainWindow::on_pushButton_clicked()
{
    this->searchWord(this->ui->searchWordLineEdit->text());
}

void    MainWindow::searchWord(QString word)
{
    int row = 0;
    int col = 0;
    bool    stop = false;

    letterVector.clear();
    letterVector.resize(0);
    for (int i = 0; i < word.size(); ++i)
    {
        t_letter letter;
        letter.row = -1;
        letter.col = -1;
        letterVector.append(letter);
    }

    while (!stop && row < this->ui->wordsTable->rowCount())
    {
        col = 0;
        while (!stop && col < this->ui->wordsTable->columnCount())
        {
            if (!this->ui->wordsTable->item(row, col)->text().isEmpty() && this->ui->wordsTable->item(row, col)->text()[0] == word[0])
            {
                this->fillVectorSlot(0, row, col);
                if (this->findNextLetter(word, 1, row, col, NONE))
                {
                    this->highlightWord();
                    stop = true;
                }
            }
            ++col;
        }
    ++row;
    }
    QListWidgetItem *item = new QListWidgetItem(word);
    if (stop)
        item->setBackgroundColor(Qt::green);
    else
        item->setBackgroundColor(Qt::red);
    this->ui->listWidget->addItem(item);
}

bool    MainWindow::findNextLetter(QString word, int currentLetter, int row, int col, e_orientation orien)
{
    bool    correctLetters = false;

    if (!correctLetters && (orien == NONE || orien == N) && row - 1 >= 0 && this->ui->wordsTable->item(row - 1, col)->text()[0] == word[currentLetter])
    {
        correctLetters = true;
        this->fillVectorSlot(currentLetter, row - 1, col);
        if (currentLetter + 1 < word.size())
            correctLetters = this->findNextLetter(word, currentLetter + 1, row - 1, col, N);
    }
    if (!correctLetters && (orien == NONE || orien == NE) && row - 1 >= 0 && col + 1 < this->ui->wordsTable->columnCount() && this->ui->wordsTable->item(row - 1, col + 1)->text()[0] == word[currentLetter])
    {
        correctLetters = true;
        this->fillVectorSlot(currentLetter, row - 1, col + 1);
        if (currentLetter + 1 < word.size())
            correctLetters = this->findNextLetter(word, currentLetter + 1, row - 1, col + 1, NE);
    }
    if (!correctLetters && (orien == NONE || orien == E) && col + 1 < this->ui->wordsTable->columnCount() && this->ui->wordsTable->item(row, col + 1)->text()[0] == word[currentLetter])
    {
        correctLetters = true;
        this->fillVectorSlot(currentLetter, row, col + 1);
        if (currentLetter + 1 < word.size())
            correctLetters = this->findNextLetter(word, currentLetter + 1, row, col + 1, E);
    }
    if (!correctLetters && (orien == NONE || orien == SE) && row + 1 < this->ui->wordsTable->rowCount() && col + 1 < this->ui->wordsTable->columnCount() && this->ui->wordsTable->item(row + 1, col + 1)->text()[0] == word[currentLetter])
    {
        correctLetters = true;
        this->fillVectorSlot(currentLetter, row + 1, col + 1);
        if (currentLetter + 1 < word.size())
            correctLetters = this->findNextLetter(word, currentLetter + 1, row + 1, col + 1, SE);
    }
    if (!correctLetters && (orien == NONE || orien == S) && row + 1 < this->ui->wordsTable->rowCount() && this->ui->wordsTable->item(row + 1, col)->text()[0] == word[currentLetter])
    {
        correctLetters = true;
        this->fillVectorSlot(currentLetter, row + 1, col);
        if (currentLetter + 1 < word.size())
            correctLetters = this->findNextLetter(word, currentLetter + 1, row + 1, col, S);
    }
    if (!correctLetters && (orien == NONE || orien == SO) && row + 1 < this->ui->wordsTable->rowCount() && col - 1 >= 0 && this->ui->wordsTable->item(row + 1, col - 1)->text()[0] == word[currentLetter])
    {
        correctLetters = true;
        this->fillVectorSlot(currentLetter, row + 1, col - 1);
        if (currentLetter + 1 < word.size())
            correctLetters = this->findNextLetter(word, currentLetter + 1, row + 1, col - 1, SO);
    }
    if (!correctLetters && (orien == NONE || orien == O) && col - 1 >= 0 && this->ui->wordsTable->item(row, col - 1)->text()[0] == word[currentLetter])
    {
        correctLetters = true;
        this->fillVectorSlot(currentLetter, row, col - 1);
        if (currentLetter + 1 < word.size())
            correctLetters = this->findNextLetter(word, currentLetter + 1, row, col - 1, O);
    }
    if (!correctLetters && (orien == NONE || orien == NO) && row - 1 >= 0 && col - 1 >= 0 && this->ui->wordsTable->item(row - 1, col - 1)->text()[0] == word[currentLetter])
    {
        correctLetters = true;
        this->fillVectorSlot(currentLetter, row - 1, col - 1);
        if (currentLetter + 1 < word.size())
            correctLetters = this->findNextLetter(word, currentLetter + 1, row - 1, col - 1, NO);
    }
    return (correctLetters);
}

void    MainWindow::highlightWord()
{
    static bool                 firstCall = true;
    static QVector<t_letter>    saveVector;

    if (!firstCall)
    {
        for (int i = 0; i < saveVector.size(); ++i)
            this->ui->wordsTable->item(saveVector[i].row, saveVector[i].col)->setBackground(Qt::gray);
    }

    for (int i = 0; i < this->letterVector.size(); ++i)
        this->ui->wordsTable->item(this->letterVector[i].row, this->letterVector[i].col)->setBackground(Qt::green);

    firstCall = false;
    saveVector = this->letterVector;
}

void    MainWindow::fillVectorSlot(int slotNumber, int row, int col)
{
    this->letterVector[slotNumber].row = row;
    this->letterVector[slotNumber].col = col;
}

void MainWindow::on_fillButton_clicked()
{
    QString     text = this->ui->fillTableTextEdit->toPlainText();
    int         row = 0;
    int         col = 0;
    bool        init = true;

    this->clearTableWidget();

    this->ui->wordsTable->setRowCount(row + 1);
    this->ui->wordsTable->setVerticalHeaderItem(row, new QTableWidgetItem(QString::number(row + 1)));
    for (int i = 0; i < text.size(); ++i)
    {
        if (text[i] != '\n')
        {
            if (init == true)
            {
                this->ui->wordsTable->setColumnCount(col + 1);
                this->ui->wordsTable->setHorizontalHeaderItem(col, new QTableWidgetItem(QString::number(col + 1)));
            }
            this->ui->wordsTable->setItem(row, col, new QTableWidgetItem(QString(text[i])));
            ++col;
        }
        else if (text[i] == '\n')
        {
            init = false;
            ++row;
            col = 0;
            this->ui->wordsTable->setRowCount(row + 1);
            this->ui->wordsTable->setVerticalHeaderItem(row, new QTableWidgetItem(QString::number(row + 1)));
        }
    }
}
