# README #

### What is this repository for? ###

* Personnal project named Hidden Word Solver
* 1.0

### How do I get set up? ###

* Compile with Qt
* For all Qt Projects Releases, don't forget to add with the .exe the following dlls:
- libgcc_s_dw2-1.dll
- libstdc++-6.dll
- libwinpthread-1.dll
- Qt5Core.dll
- Qt5Gui.dll
- Qt5Widgets.dll
- platforms/qminimal.dll
- platforms/qwindows.dll

### Notes ###

* Sources are named CrossedWordSolver but is actually a progrma for hidden words game