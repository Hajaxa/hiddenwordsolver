#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector>

struct  t_letter
{
    int row;
    int col;
};

enum e_orientation
{
    N,
    NE,
    E,
    SE,
    S,
    SO,
    O,
    NO,
    NONE
};

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void    resizeTableWidget(int row, int col);
    void    clearTableWidget();
    void    searchWord(QString word);
    bool    findNextLetter(QString word, int currentLetter, int row, int col, e_orientation orien);
    void    fillVectorSlot(int slotNumber, int row, int col);
    void    highlightWord();


private slots:
    void on_horizontalPushButton_clicked();

    void on_verticalPushButton_clicked();

    void on_pushButton_clicked();

    void on_fillButton_clicked();

private:
    Ui::MainWindow *ui;
    QVector<t_letter> letterVector;
};

#endif // MAINWINDOW_H
